from typing import List


def main():
    with open("../data/02.dat", "r") as f:
        data = [int(x) for x in f.read().split(",")]
    A = partA(data)
    B = partB(data)
    print(f"Part A: {A}")
    print(f"Part B: {B}")


def partA(data: List[int]) -> int:
    return process(data, 12, 2)


def partB(data: List[int]) -> int:
    target = 19690720
    for noun in range(100):
        for verb in range(100):
            if process(data, noun, verb) == target:
                return (100 * noun) + verb


def process(data: List[int], noun: int, verb: int) -> int:
    data = data[:]  # Copy the list so we can modify it freely
    data[1] = noun
    data[2] = verb
    index = 0
    while True:
        opcode = data[index]
        A = data[index + 1]
        B = data[index + 2]
        X = data[index + 3]
        if opcode is 1:
            data[X] = data[A] + data[B]
        elif opcode is 2:
            data[X] = data[A] * data[B]
        else:
            break
        index += 4
    return data[0]


main()
