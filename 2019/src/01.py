def main():
    with open("../data/01.dat", "r") as f:
        data = [int(x) for x in f]
    A = sum([fuelA(x) for x in data])
    print(f"Part A: {A}")
    B = sum([fuelB(x) for x in data])
    print(f"Part B: {B}")

def fuelA(mass: int) -> int:
    return (mass // 3) - 2

def fuelB(mass: int) -> int:
    result = fuelA(mass)
    if result > 0:
        return result + fuelB(result)
    else:
        return 0

main()
