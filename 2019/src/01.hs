main = do
    input <- readFile "../data/01.dat"
    putStrLn $ "Part A: " ++ show (partA (parseInput input))
    putStrLn $ "Part B: " ++ show (partB (parseInput input))

parseInput :: String -> [Int]
parseInput input = map (read::String->Int) (lines input)

partA :: [Int] -> Int
partA x = sum (map fuel x)

fuel :: Int -> Int
fuel x = subtract 2 (x `div` 3)

partB :: [Int] -> Int
partB x = sum (map sumFuel x)

sumFuel :: Int -> Int
sumFuel x = sum (drop 1 (takeWhile (> 0) (iterate fuel x)))

