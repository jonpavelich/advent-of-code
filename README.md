# Advent of Code

## What is this?
My solutions to the [Advent of Code](https://adventofcode.com/) challenges.

## Goal
Just complete every challenge on the release day. In past years I've lost motivation pretty quickly as I get busy with final exams and travel home for the holidays. So far 2020 has been a bit of a [dumpster fire](https://hey.science/dumpster-fire) but I'm also finally graduating and not travelling - this could be the year to make it through all the challenges!

## Solutions

- [2020](2020/src) - Ongoing
- [2019](2019/src) - Incomplete
- [2018](2018/src) - Incomplete
- [2017](2017/src) - Incomplete
