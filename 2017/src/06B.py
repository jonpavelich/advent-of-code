import sys

banks = []
history = {}
verbose = False

def redistribute():
	max = banks[0]
	index = 0
	for i in range(len(banks)):
		if banks[i] > max:
			max = banks[i]
			index = i
	
	if verbose:
		print("Select "+str(i)+" (has "+str(max)+") for redistribution")
		
	reserve = banks[index]
	banks[index] = 0
	
	while reserve > 0:
		index = (index + 1) % len(banks)
		banks[index] += 1
		reserve -= 1
	
def main():
	global banks
	for m in sys.stdin.readline().split():
		banks.append(int(m))

	if("-v" in sys.argv):
		global verbose
		verbose = True
		
	print("Solving")
	sys.stdout.flush()
	
	steps = 0
	while str(banks) not in history:
		history[str(banks)] = steps
		redistribute()
		steps += 1
		if steps % 1000 == 0 and not verbose:
			sys.stdout.write(".")
			sys.stdout.flush()
			
	print()
	print(str(steps)+" steps taken")
	print(str(steps - history[str(banks)])+" step cycle identitified")
	
main()