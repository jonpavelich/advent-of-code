import sys
	
def is_valid(passwd):
	d = {}
	for w in passwd.split():
		s = ''.join(sorted(w)) # handle anagrams
		if s in d:
			return False
		d[s] = True
	return True

def check_list():
	valid_count = 0
	total_count = 0
	for line in sys.stdin:
		total_count += 1
		if is_valid(line.strip()):
			valid_count += 1
			sys.stdout.write("+")
		else:
			sys.stdout.write(".")
	print()
	print(str(valid_count)+" valid of "+str(total_count)+" total")

check_list()