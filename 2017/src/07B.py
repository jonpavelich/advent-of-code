import sys

tree = {}
node_weight = {}	
verbose = False
		
def v_print(msg):
	if verbose:
		print(msg)
		
def balance(root):
	v_print("inspecting "+root)
	# add our node's weight to the cumulative weight of this branch
	weight = node_weight[root]
	
	# if we have no children, we are balanced and have only our own weight
	if len(tree[root]) is 0:
		v_print(root+": no children. return "+str(weight))
		return weight
	
	# find weight of each child, adding it to the sum and set up results to check for balance
	weights = []
	map = {}
	for c in tree[root]:
		w = balance(c)
		weight += w
		weights.append(w)
		map[w] = c
	
	unique = [x for x in weights if weights.count(x) == 1]
	if len(unique) is 0:
		v_print(root+": all children balanced. return "+str(weight))
		return weight
	w_unique = unique[0]
	w_common = [x for x in weights if x != w_unique][0]
	w_unique_node = node_weight[map[w_unique]]
	
	w_diff = w_common - w_unique
	print("node "+str(map[w_unique])+" is "+str(node_weight[map[w_unique]])+" but should be "+str(w_unique_node + w_diff))
	sys.exit(0)
	
def main():
	parent = {}

	if "-v" in sys.argv:
		global verbose
		verbose = True
		
	for line in sys.stdin:
		base = line.split()[0]
		weight = int(line.split("(")[1].split(")")[0])
		node_weight[base] = weight
		if base not in parent:
			parent[base] = None
		if "->" in line:
			holding = line.split("->")[-1].strip().split(", ")
			for h in holding:
				parent[h] = base

	# It's a tree, just pick a node and walk to the root
	root = next(iter(parent))
	while parent[root] is not None:
		root = parent[root]
		
	print("Root: "+root)
	
	for c in parent.keys():
		p = parent[c]
		if p not in tree:
			tree[p] = []
		tree[p].append(c)
		if c not in tree:
			tree[c] = []
	
	balance(root)
	
main()