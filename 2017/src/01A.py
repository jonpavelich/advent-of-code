import sys

line = sys.stdin.readline().strip()
seq = []
for c in line:
    seq.append(int(c))
seq.append(seq[0])

print("Analysing "+str(seq))

total = 0
for i in range(len(seq) - 1):
    if seq[i] == seq[i+1]:
        total += seq[i]

print("Result: "+str(total))
