import math
import sys

map = {} # tuple<int x, int y>, int val
target = 0

def surrounding_sum(x, y):
	sum = 0
	for i in range(-1,2):
		for j in range(-1,2):
			if (x+i, y+j) in map:
				sum += map[(x+i, y+j)]
	if sum > target:
		print("+")
		print("Located sum larger than target at position ("+str(x)+","+str(y)+")")
		print("Sum: "+str(sum))
		sys.exit(0)
	#print("("+str(x)+","+str(y)+"): "+str(sum))
	return sum
	
def cycle(edge_width):
	offset = int((edge_width - 2) / 2)
	x_pos = offset
	y_pos = -1 * offset
	x_pos += 1 # move to cycle we need to fill
	
	# up
	for i in range(edge_width - 2):
		map[(x_pos, y_pos)] = surrounding_sum(x_pos, y_pos)
		y_pos += 1
	
	# left
	for i in range(edge_width - 1):
		map[(x_pos, y_pos)] = surrounding_sum(x_pos, y_pos)
		x_pos -= 1
	
	# down
	for i in range(edge_width - 1):
		map[(x_pos, y_pos)] = surrounding_sum(x_pos, y_pos)
		y_pos -= 1
	
	# right
	for i in range(edge_width):
		map[(x_pos, y_pos)] = surrounding_sum(x_pos, y_pos)
		x_pos += 1
	
def solve():
	global target
	target = int(sys.stdin.readline().strip())
	map[(0,0)] = 1
	edge = 3
	while True:
		cycle(edge)
		sys.stdout.write(".")
		edge += 2
		
solve()