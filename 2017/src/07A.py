import sys

parent = {}

for line in sys.stdin:
	base = line.split()[0]
	if base not in parent:
		parent[base] = None
	if "->" in line:
		holding = line.split("->")[-1].strip().split(", ")
		for h in holding:
			parent[h] = base

# It's a tree, just pick a node and walk to the root
root = next(iter(parent))
while parent[root] is not None:
	root = parent[root]
	
print(root)