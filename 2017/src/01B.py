import sys

line = sys.stdin.readline().strip()
seq = []
for c in line:
    seq.append(int(c))
seq = seq*2

offset = int(len(line)/2)
print("Analysing "+str(seq)+" using offset "+str(offset))
total = 0
for i in range(int(len(seq)/2)):
    if seq[i] == seq[i+offset]:
        total += seq[i]

print("Result: "+str(total))
