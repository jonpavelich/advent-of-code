import sys

list = []
verbose = False

def in_range(index):
	return index >= 0 and index < len(list)

def print_hl(index):
	if verbose:
		sys.stdout.write("[")
		for i in range(len(list)):
			sys.stdout.write(" ")
			if i == index:
				sys.stdout.write("("+str(list[i])+")")
			else:
				sys.stdout.write(" "+str(list[i])+" ")
		sys.stdout.write(" ]\n")
	
def jump(index):
	target = index + list[index]
	if in_range(index):
		if list[index] >=3:
			list[index] -= 1
		else:
			list[index] += 1
	return target
	
def main():
	global list
	for line in sys.stdin:
		list.append(int(line))

	if("-v" in sys.argv):
		global verbose
		verbose = True
		
	print("Solving")
	sys.stdout.flush()
	
	steps = 0
	index = 0
	while in_range(index):
		print_hl(index)
		index = jump(index)
		steps += 1
		if steps % 1000 == 0 and not verbose:
			sys.stdout.write(".")
			sys.stdout.flush()
			
	print()
	print(str(steps)+" steps taken")
	
main()