import sys

verbose = False

def v_print(msg):
	if verbose:
		print(msg)
		
def v_write(msg):
	if verbose:
		sys.stdout.write(msg)
		
def main():
	stack = []
	score = 0
	garbage = False
	cancel = False
	total = 0
	
	if "-v" in sys.argv:
		global verbose
		verbose = True

	line = sys.stdin.readline().strip()
	for c in line:
		v_write(str(c)+": ")
		if cancel:
			cancel = False
			v_print("cancel, clear cancel")
			continue
		
		if c == '!':
			cancel = True
			v_print("set cancel")
			continue
		
		if c == '>':
			garbage = False
			v_print("clear garbage")
			continue
			
		if garbage:
			v_print("garbage")
			continue
			
		if c == '<':
			garbage = True
			v_print("set garbage")
			continue
			
		if c == '{':
			stack.append('{')
			score += 1
			v_print("start group (set score "+str(score)+")")
			continue
			
		if c == '}':
			stack.pop()
			total += score
			v_print("end group (add score "+str(score)+", total is "+str(total)+")")
			score -= 1
			continue
			
	print("Total score: "+str(total))
	
main()