import sys

check = 0
for line in sys.stdin:
	line = line.strip().split()
	for i in range(len(line)):
		for j in range(i+1, len(line)):
			a = int(line[i])
			b = int(line[j])
			if a > b:
				s = b
				l = a
			else:
				s = a
				l = b
			if l % s == 0:
				check += int(l/s)
print(str(check))