import sys

map = {}

def get_map(x):
	if x not in map:
		map[x] = 0
	return map[x]

# increase target by sign*amount if "comp_target operator comp_amount" evaluates to True
def exec_op(target, sign, amount, comp_target, operator, comp_amount):
	comp = eval(str(get_map(comp_target)) + operator + str(comp_amount))
	if comp:
		map[target] = get_map(target) + (sign * amount)

def main():
	for line in sys.stdin:
		line = line.strip().split()
		target = line[0]
		sign = 1 if line[1] == "inc" else -1
		amount = int(line[2])
		comp_target = line[4]
		operator = line[5]
		comp_amount = int(line[6])
		
		exec_op(target, sign, amount, comp_target, operator, comp_amount)
		if "-v" in sys.argv:
			print(str(map))
	
	max = map[next(iter(map))]
	for k,v in map.items():
		if v > max:
			max = v
	
	print(str(max))
	
main()