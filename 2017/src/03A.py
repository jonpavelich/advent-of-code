import sys
import math

def abs(a, b):
	res = a
	if res < 0:
		res *= -1
	if b < 0:
		res += -1 * b
	else:
		res += b
	return res
	
def go():
	target = int(sys.stdin.readline())
	print("targeting "+str(target))
	if target == 1:
		print("result is 0")
		return
	base = int(math.sqrt(target))
	if base % 2 == 0:
		base -= 1
	result = base * base
	print("using base "+str(base)+" for start point of "+str(result))

	dist_X = int(base / 2)
	dist_Y = int(-1 * base / 2)
	step = base
	
	print("At ["+str(dist_X)+","+str(dist_Y)+"], stepping right 1")
	dist_X += 1
	result += 1

	sys.stdout.write("At ["+str(dist_X)+","+str(dist_Y)+"], working up: ")
	if result + step < target:
		result += step
		dist_Y += step
		print(str(step)+" to "+str(result))
	else:
		step = target - result
		result = target
		dist_Y += step
		print(str(step)+" to "+str(result))
		print("At ["+str(dist_X)+","+str(dist_Y)+"]")
		print("result is "+str(abs(dist_X, dist_Y)))
		return
		
	step += 1
	
	sys.stdout.write("At ["+str(dist_X)+","+str(dist_Y)+"], working left: ")
	if result + step < target:
		result += step
		dist_X -= step
		print(str(step)+" to "+str(result))
	else:
		step = target - result
		result = target
		dist_X -= step
		print(str(step)+" to "+str(result))
		print("At ["+str(dist_X)+","+str(dist_Y)+"]")
		print("result is "+str(abs(dist_X, dist_Y)))
		return
		
	sys.stdout.write("At ["+str(dist_X)+","+str(dist_Y)+"], working down: ")
	if result + step < target:
		result += step
		dist_Y -= step
		print(str(step)+" to "+str(result))
	else:
		step = target - result
		result = target
		dist_Y -= step
		print(str(step)+" to "+str(result))
		print("At ["+str(dist_X)+","+str(dist_Y)+"]")
		print("result is "+str(abs(dist_X, dist_Y)))
		return

	sys.stdout.write("At ["+str(dist_X)+","+str(dist_Y)+"], working right: ")
	if result + step < target:
		result += step
		dist_X += step
		print(str(step)+" to "+str(result))
	else:
		step = target - result
		result = target
		dist_X += step
		print(str(step)+" to "+str(result))
		print("At ["+str(dist_X)+","+str(dist_Y)+"]")
		print("result is "+str(abs(dist_X, dist_Y)))
		return
		
go()