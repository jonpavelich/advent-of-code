import sys

check = 0
for line in sys.stdin:
	line = line.strip().split()
	min = int(line[0])
	max = int(line[0])
	for c in line:
		i = int(c)
		if i < min:
			min = i
		if i > max:
			max = i
	check += max - min
	print("["+str(max-min)+"]: "+str(line))
print("Checksum: "+str(check))