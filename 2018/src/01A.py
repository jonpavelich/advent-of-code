import sys

def main():
    freq = 0
    for line in sys.stdin:
        freq += int(line)
    print(freq)

if __name__ == '__main__':
    main()
