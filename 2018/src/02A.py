import sys
from collections import Counter

def main():
    two_count = 0
    three_count = 0
    
    for line in sys.stdin:
        line = line.rstrip()
        counter = Counter(line)
        char_set = set(line)
        two = False
        three = False
        for char in char_set:
            if counter[char] == 2:
                two = True
            if counter[char] == 3:
                three = True
        if two:
            two_count += 1
        if three:
            three_count += 1

    print(two_count * three_count)
        
        
if __name__ == '__main__':
    main()
