import sys

def main():
    adjustments = list()
    freq_history = set()

    for line in sys.stdin:
        adjustments.append(int(line))

    freq = 0
    while True:
        for adj in adjustments:
            if freq in freq_history:
                print(freq)
                return
            freq_history.add(freq)
            freq += adj


if __name__ == '__main__':
    main()
