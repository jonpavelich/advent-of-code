from typing import List
import logging


def partA(data: List[str]) -> int:
    valid = 0
    for line in data:
        range_str, character, password = line.split(" ")
        range_lower, range_upper = [int(x) for x in range_str.split("-")]
        character = character[0]
        actual = len([x for x in password if x == character])
        msg = f"Parsed line ({line.rstrip()}) as requiring {range_lower} to {range_upper} ({character}) characters: "
        if actual >= range_lower and actual <= range_upper:
            valid += 1
            logging.debug(f"{msg}: VALID")
        else:
            logging.debug(f"{msg}: invalid")
    return valid


def partB(data: List[str]) -> int:
    valid = 0
    for line in data:
        positions, character, password = line.split(" ")
        p1, p2 = [int(x) for x in positions.split("-")]
        character = character[0]
        msg = f"Parsed line ({line.rstrip()}) as requiring ({character}) in index {p1} or {p2}: "
        if len([x for x in [p1, p2] if password[x - 1] == character]) == 1:
            valid += 1
            logging.debug(f"{msg}: VALID")
        else:
            logging.debug(f"{msg}: invalid")
    return valid


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    with open("../data/02.dat", "r") as f:
        data = [x for x in f]
    print(partA(data))
    print(partB(data))


main()
