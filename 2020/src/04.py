from typing import List
import logging


def partA(data: List[str]) -> int:
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]  # no cid
    valid = 0
    for line in data:
        passport = {
            i[0]: i[1] for i in [x.split(":") for x in line.split(" ") if len(x)]
        }
        missing = [x for x in required if x not in passport]
        if len(missing) == 0:
            valid += 1
    return valid


def is_valid(key: str, value: str) -> bool:
    if key == "byr":
        return value.isdigit() and 1920 <= int(value) <= 2002
    elif key == "iyr":
        return value.isdigit() and 2010 <= int(value) <= 2020
    elif key == "eyr":
        return value.isdigit() and 2020 <= int(value) <= 2030
    elif key == "hgt":
        num, unit = value[:-2], value[-2:]
        return (unit == "cm" and num.isdigit() and 150 <= int(num) <= 193) or (
            unit == "in" and num.isdigit() and 59 <= int(num) <= 76
        )
    elif key == "hcl":
        return (
            len(value) == 7
            and value[0] == "#"
            and all([x.isdigit() or "a" <= x <= "f" for x in value[1:]])
        )
    elif key == "ecl":
        valid = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        return value in valid
    elif key == "pid":
        return len(value) == 9 and value.isdigit()
    elif key == "cid":
        return True
    return False


def partB(data: List[str]) -> int:
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]  # no cid
    valid = 0
    for line in data:
        passport = {
            i[0]: i[1] for i in [x.split(":") for x in line.split(" ") if len(x)]
        }
        missing = [x for x in required if x not in passport]
        if len(missing) == 0 and all([is_valid(k, v) for k, v in passport.items()]):
            valid += 1
    return valid


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    with open("../data/04.dat", "r") as f:
        data: List[str] = []
        passport = ""
        for line in f:
            if len(line.rstrip()) == 0:
                data.append(passport.rstrip())
                passport = ""
            else:
                passport += line.rstrip() + " "

    print(partA(data))
    print(partB(data))


main()
