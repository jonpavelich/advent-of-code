from typing import List
import logging


def partA(data: List[int], goal: int) -> int:
    data = sorted(data)

    counter = 0

    for i_right in range(len(data)):
        for i_left in range(len(data)):
            left, right = data[i_left], data[i_right]
            counter += 1
            if left + right == goal:
                logging.info(f"found ({left}, {right})")
                logging.debug(f"partA took {counter} steps")
                return left * right
            elif left + right > goal:
                break  # exceeded goal, skip checking larger numbers
            # else keep looking

    raise ValueError(f"No pair of numbers sums to {goal}")


def partB(data: List[int], goal: int) -> int:
    data = sorted(data)

    for i in range(len(data)):
        try:
            pair = partA(data[:i] + data[i + 1 :], goal - data[i])
            logging.info(f"found matching pair for ({data[i]})")
            return pair * data[i]
        except ValueError:
            pass

    raise ValueError(f"No trio of numbers sums to {goal}")


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    with open("../data/01.dat", "r") as f:
        data = [int(x) for x in f]
    print(partA(data, 2020))
    print(partB(data, 2020))


main()
