from typing import List
import logging


def partA(data: List[str]) -> int:
    result = 0
    for line in data:
        row = int("".join(["0" if x == "F" else "1" for x in line[:7]]), 2)
        col = int("".join(["0" if x == "L" else "1" for x in line[7:]]), 2)
        result = max(result, row * 8 + col)
    return result


def partB(data: List[str]) -> int:
    seats = []
    for line in data:
        row = int("".join(["0" if x == "F" else "1" for x in line[:7]]), 2)
        col = int("".join(["0" if x == "L" else "1" for x in line[7:]]), 2)
        seats.append(row * 8 + col)
    seats.sort()
    for i in range(1, len(seats)):
        if seats[i] - seats[i - 1] > 1:
            return seats[i] - 1
    raise ValueError("No seat found")


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    with open("../data/05.dat", "r") as f:
        data = [x.rstrip() for x in f]
    print(partA(data))
    print(partB(data))


main()
