from typing import List, Tuple
import logging


def partA(data: List[List[bool]], x_step: int, y_step: int) -> int:
    width, height = len(data[0]), len(data)
    x, y = 0, 0
    count = 0

    logging.debug(f"Data is ({width} x {height})")

    while y < height:
        prev_x, prev_y, prev_count = x, y, count
        if data[y][x]:
            count += 1
        y += y_step
        x = (x + x_step) % width
        logging.debug(
            f"Moved from ({prev_x},{prev_y}) to ({x},{y}): {'no tree' if count == prev_count else 'TREE'}"
        )

    return count


def partB(data: List[List[bool]], steps: List[Tuple[int, int]]) -> int:
    total = 1
    for step in steps:
        x, y = step[0], step[1]
        trees = partA(data, x, y)
        total *= trees
        logging.info(f"Step size ({x},{y}) hit {trees} trees")
    return total


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    with open("../data/03.dat", "r") as f:
        data = [[x == "#" for x in line.rstrip()] for line in f]
    print(partA(data, 3, 1))
    print(partB(data, [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]))


main()
